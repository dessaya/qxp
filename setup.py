#!/usr/bin/env python
from distutils.core import setup

ld="""qxp is a collection of utility functions and classes on top of the
Pygame framework."""

setup(
	name='qxp',
	version='0.1',
	author='Diego Essaya',
	author_email='diego@qb9.net',
	url='http://www.qb9.net/qxp/',
	description="QB9's eXtensions for Pygame",
	long_description=ld,
	license='MIT License',
	packages=['qxp', 'qxp.widgets'],
	platforms=['all']
)

