# qxp - QB9's eXtensions for Pygame

The qxp package implements a collection of utility functions and classes on top
of the [Pygame](http://www.pygame.org) framework.
It was used at [QB9](http://www.qb9.net) to quickly prototype games.

It is distributed under the [MIT License](http://www.opensource.org/licenses/mit-license.php),
which means that you are free to use it in your own free or commercial projects, and
also modify and redistribute it.

## Installing

If you want to use qxp without installing it system-wide, you can copy the
qxp/ directory inside your project's root directory.  Your project tree should
look like this:

```
your_script.py
qxp/
	__init__.py
	...etc..
```

If you want to install system-wide, just use the setup.py script:

```
python setup.py install
```