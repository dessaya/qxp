# This file is part of the qxp package.
# Author: Diego Essaya <diego@qb9.net>
# Copyright (c) 2007 QB9 Entertainment
# License: See COPYING

"""Wrapper module for `pygame.display`.

Features:

* Provides a workaround for the fullscreen problem in Pygame 1.7.1.
* Maintains a global list of dirty rects, which is passed to
  `pygame.display.update()` in the `qxp.display.update()` wrapper method.

:Variables:
	- `surface`: Reference to the surface created with `qxp.display.init()`.
	- `dirtyrects`: The current list of dirty rects. Use
	  `qxp.display.markdirty()` or `Widget.markdirty()` to add a dirty rect.
"""

import pygame
import cursor

__docformat__ = 'restructuredtext en'

surface = None
dirtyrects = []
_flags = 0
_renderclock = pygame.time.Clock()

def init(name='Pygame + qxp', size=(800,600), flags=0):
	"""Wrapper for `pygame.display.set_mode()` and `pygame.display.set_caption()`.
	
	:Returns: the screen surface, which can also be accessed with
		`qxp.display.surface` or the usual `pygame.display.get_surface()`.

	:Parameters:
		- `name`: The caption of the window.
		- `size`: The size, in pixels.
		- `flags`: The desired set of flags, as would be passed to `pygame.display.set_mode()`.
	"""

	global _flags, surface
	_flags = flags
	surface = pygame.display.set_mode(size, flags)
	pygame.display.set_caption(name)
	return surface

def toggle_fullscreen():
	"""Replacement for `pygame.display.toggle_fullscreen()` that actually works =)."""

	global _flags
	size = get_rect().size
	caption = pygame.display.get_caption()[0]
	# Commented out due to a bug (in pygame?):
	#pygame.display.quit()
	pygame.display.init()
	markdirty()
	return init(caption, size, _flags ^ pygame.FULLSCREEN)

def update():
	"""Wrapper for `pygame.display.update()`, automatically called from
	`WidgetManager.loop()`.
	
	This method performs several actions:

	* Measures the FPS rate. You can display it using `FPSWidget`.
	* Automatically renders the cursor (if necessary).
	* Calls `pygame.display.update()` with the current list of dirty rects.
	* Clears the list of dirty rects.
	"""

	global _renderclock, dirtyrects
	_renderclock.tick()
	cursor.render()
	pygame.display.update(dirtyrects)
	del dirtyrects[:]

def markdirty(dirtyrect=None):
	"""Add a new dirty rect to the list.

	This method computes the union of all the dirty rects that collide,
	to ensure that no area of the screen will be rendered twice.
	
	Usually it is more convenient to use the `Widget.markdirty()` shortcut.
	"""

	global dirtyrects
	if dirtyrect is None:
		dirtyrect = get_rect()
	else:
		dirtyrect = dirtyrect.clip(get_rect())
	indices = dirtyrect.collidelistall(dirtyrects)
	removed = 0
	for i in indices:
		i -= removed
		rect = dirtyrects.pop(i)
		removed += 1
		dirtyrect.union_ip(rect)
	dirtyrects.append(dirtyrect)

def get_rect():
	"""Shortcut for ``qxp.display.surface.get_rect()``.
	
	:Returns: The rect associated to the current display surface.
	"""

	global surface
	return surface.get_rect()

