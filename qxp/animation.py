# This file is part of the qxp package.
# Author: Diego Essaya <diego@qb9.net>
# Copyright (c) 2007 QB9 Entertainment
# License: See COPYING

"""Generic animations subsystem.

The animations module implements a set of classes that are strongly
inspired in the Panda3D interval system; so if you are familiar with
Panda3D, you will find similar concepts here.

An animation is an object that modifies one or more state variables over
time, for a given duration.  For example, the RectAnimation can modify
the position and/or size of a widget's rect attribute.

The `Animation` class is the generic base class for all animations.  All
derived classes implement the desired behavior by overriding the
``timestep()`` method.

To start playing an animation, it must be added to the `AnimationManager`
instance of the `WidgetManager`.  When the animation finishes, it is
automatically removed from the animation manager.

This is the list of built-in special animations:

* `RectAnimation`: Able to move and/or resize a widget.
* `Wait`: Do nothing for a given duration.
* `Sequence`: Play a set of animations in sequence.
* `Parallel`: Play a set of animations in parallel.
* `Func`: Call an arbitrary function when the animation is triggered.

You can combine this basic set to produce complex effects.  For example::

	wm.animations.add(
		Sequence(
			Parallel(
				widget1.RectAnimation(1.2, rect1),
				Sequence(
					Wait(0.6),
					widget2.RectAnimation(0.6, rect2),
				),
			),
			Func(sound_sample.play),
		)
	)
"""

import pygame

__docformat__ = 'restructuredtext en'

class AnimationManager(list):
	"""Animation manager (duh!).

	This object maintains a dynamic collection of animations and updates their
	status on each timestep event.  Every `WidgetManager` instance has its own
	``AnimationManager`` instance, namely `WidgetManager.animations`.

	Use `add()` to add an animation instance.  Animations are automatically
	removed when they finish.  Use `remove()` to remove an animation before
	it has finished.
	"""

	def timestep(self, step):
		"""Update the status of all contained animations, and remove the
		ones that have finished.
		"""

		if len(self) == 0:
			return
		for anim in self:
			anim.timestep(step)
		self[:] = [ a for a in self if not a.finished ]

	def add(self, anim):
		"""Add a new animation and start it.
		
		The `Animation.start()` callback will be executed.
		"""

		self.append(anim)
		anim.start()

class Animation(object):
	"""Base class for all animations.
	
	:IVariables:
		- `duration`: The duration, in seconds, of the animation.
		- `t`: The current position, in seconds, of the animation, between
		  0.0 and ``duration``.
		- `started`: True if the animation has been triggered.
		- `finished`: True when the animation has finished.  Check that this
		  value is False before calling `AnimationManager.remove()`.
	"""

	def __init__(self, duration):
		"""Create an Animation instance.

		:Arguments:
			- `duration`: Duration of the animation, in seconds.
		"""

		self.finished = False
		self.started = False
		self.duration = float(duration)
		self.t = 0.0

	def start(self):
		"""Callback executed when the animation is triggered.

		This default callback sets `started` ``= True``.
		"""

		self.started = True

	def timestep(self, step):
		self.t += step

class Wait(Animation):
	"""Animation that implements a pause."""

	def timestep(self, step):
		Animation.timestep(self, step)
		if self.t >= self.duration:
			self.t = self.duration
			self.finished = True

class Func(Animation):
	"""Function call animation.

	This special animation has zero duration.  When started, it calls an
	arbitrary function, optionally passing some arguments, and then
	it instantly finishes.
	"""

	def __init__(self, func, *args):
		Animation.__init__(self, 0)
		self.func = func
		self.args = args
	
	def timestep(self, step):
		self.func(*self.args)
		self.finished = True

class LerpAnimation(Animation):
	"""Linear interpolation base animation class.

	The word Lerp is short for "linearly interpolate".  This animation
	provides the basic methods common to all animations that interpolate
	a property (a color, a position, a size, etc.) between two given values.
	"""

	def __init__(self, duration, dest=1.0, src=0.0, easein=False, easeout=False):
		"""Create a LerpAnimation instance.

		:Arguments:
			- `duration`: The duration of the animation, in seconds.
			- `dest`: Destination: the end value of the interpolation.
			- `src`: Source: the start value of the interpolation.
			- `easein`: If set, the lerp begins smoothly.
			- `easeout`: If set, the lerp ends smoothly.
		"""

		Animation.__init__(self, duration)
		self.src = float(src)
		self.dest = float(dest)
		self.dist = self.dest - self.src
		self.easein = easein
		self.easeout = easeout
	
	def timestep(self, step):
		Animation.timestep(self, step)
		rt = self.t / self.duration
		if self.easein and not self.easeout:
			rt = (3.0 * rt ** 2 - rt ** 3) / 2.0
		elif not self.easein and self.easeout:
			rt = (3.0 * rt - rt ** 3) / 2.0
		elif self.easein and self.easeout:
			rt = 3.0 * rt ** 2 - 2.0 * rt ** 3
		if self.t >= self.duration:
			self.finished = True
			return self.dest
		return rt * self.dist + self.src

class RectAnimation(LerpAnimation):
	"""Interpolate the rect value of a widget between two given Rects.
	
	Use this to implement an animation that moves and/or resizes a widget.
	"""

	def __init__(self, widget, duration, destrect, srcrect=None, easein=False, easeout=False):
		"""Create a RectAnimation instance.

		:Arguments:
			- `widget`: The widget whos ``rect`` member will be modified.
			- `duration`: The duration of the animation, in seconds.
			- `destrect`: End value for the rect.
			- `srcrect`: Start value for the rect.  If not set, the widget's
			  current ``rect`` is used.
		"""

		LerpAnimation.__init__(self, duration, 1.0, 0.0, easein, easeout)
		self.srcrect = None
		if srcrect:
			self.srcrect = pygame.Rect(srcrect)
		self.destrect = pygame.Rect(destrect)
		self.widget = widget

		self.dw = None
		self.dh = None
		self.dx = None
		self.dy = None

	def start(self):
		LerpAnimation.start(self)
		if self.srcrect is None:
			self.srcrect = pygame.Rect(self.widget.rect)
		else:
			self.widget.updaterect(self.srcrect)
	
		self.dw = self.destrect.width - self.srcrect.width
		self.dh = self.destrect.height - self.srcrect.height
		self.dx = self.destrect.left - self.srcrect.left
		self.dy = self.destrect.top - self.srcrect.top

	def timestep(self, step):
		t = LerpAnimation.timestep(self, step)
		if self.finished:
			self.widget.updaterect(self.destrect)
			return
		rect = pygame.Rect(self.srcrect)
		rect.width += self.dw * t
		rect.height += self.dh * t
		rect.left += self.dx * t
		rect.top += self.dy * t
		self.widget.updaterect(rect)

class Sequence(Animation, list):
	"""Sequence of animations.

	This is a special animation that executes a group of animations in
	sequence.  Use the list methods to append animations before starting it.
	"""

	def __init__(self, *anims):
		list.__init__(self)
		self.activeanim = None
		Animation.__init__(self, 0.0)
		self += anims
		self.iter = None

	def __add__(self, anims):
		for anim in anims:
			self.append(anim)

	def append(self, anim):
		list.append(self, anim)
		self.duration += anim.duration

	def timestep(self, step):
		Animation.timestep(self, step)
		while self.activeanim is not None:
			self.activeanim.timestep(step)
			if not self.activeanim.finished: return
			self.startnext()
		self.finished = True

	def startnext(self):
		try:
			self.activeanim = self.iter.next()
			self.activeanim.start()
		except StopIteration:
			self.activeanim = None

	def start(self):
		Animation.start(self)
		self.iter = (anim for anim in self)
		self.startnext()

	def __repr__(self):
		return '<Sequence %s: %.1fs>' % (list.__repr__(self), self.duration)

class Parallel(Animation, list):
	"""Collection of animations that will start at the same time.

	This is a special animation that executes a group of animations in
	parallel; ie, all animations start at the same time.  Use the list
	methods to append animations before starting it.
	"""

	def __init__(self, *anims):
		list.__init__(self)
		Animation.__init__(self, 0.0)
		self += anims

	def __add__(self, anims):
		for anim in anims:
			self.append(anim)

	def append(self, anim):
		list.append(self, anim)
		self.duration = max(self.duration, anim.duration)

	def timestep(self, step):
		Animation.timestep(self, step)
		flag = True
		for anim in self:
			if anim.finished: continue
			anim.timestep(step)
			if not anim.finished:
				flag = False
		self.finished = flag

	def start(self):
		Animation.start(self)
		for anim in self:
			anim.start()

	def __repr__(self):
		return '<Parallel %s: %.1fs>' % (list.__repr__(self), self.duration)

