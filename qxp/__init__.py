"""
**qxp** is a collection	of utility functions and classes built on top of the
Pygame_ framework.  It is currently in development with the objective of
creating an easy to use and portable 2D game engine.  It has been used by `QB9
Entertainment`_, to produce several game prototypes.  It is distributed under
the `MIT License`_, which means that you are free to use it in your own free or
commercial projects, and also modify and redistribute it.

.. _Pygame: http://www.pygame.org
.. _`QB9 Entertainment`: http://www.qb9.net
.. _`MIT License`: http://www.opensource.org/licenses/mit-license.php

:Author: Diego Essaya
:Contact: <diego@qb9.net>
:Organization: QB9 Entertainment (http://www.qb9.net)
:Copyright: ``(c)`` 2007 QB9 Entertainment
:License: MIT License

To begin using **qxp**, just import the package from your python script
after initializing pygame.  The following example is a minimal script
that generates a blank screen::

	import pygame, qxp
	pygame.init()
	qxp.display.init()
	wm = qxp.WidgetManager()
	wm.loop()

For an introduction to **qxp**, check out the tutorials_.  It is also
recommended to read the documentation for at least the `WidgetManager` and
`Widget` classes.  Have in mind that **qxp** is not meant as a replacement
for Pygame, but just an extension.  Thus, some knowledge on how to do things
in Pygame is still required.

.. _tutorials: ../../tutorials/

Note that although all classes are defined in submodules inside the ``qxp``
package, in your user script it will be enough to use an ``import qxp`` or
``from qxp import *`` statement, since the ``qxp`` package imports all the
class definitions into its own namespace.
"""

__version__ = '0.1'
__docformat__ = 'restructuredtext en'

import display
import cursor
import events
from layer import Layer
from wm import WidgetManager
from menu import Menu
from animation import *
from widgets import *
from effects import *

