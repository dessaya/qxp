# This file is part of the qxp package.
# Author: Diego Essaya <diego@qb9.net>
# Copyright (c) 2007 QB9 Entertainment
# License: See COPYING

"""Utility classes to create special effects."""

import qxp

__docformat__ = 'restructuredtext en'

class FadeAnimation(qxp.LerpAnimation):
	"""Fade in/out effect.

	This animation creates a FillWidget that covers the entire screen and
	adds it to the WidgetManager in the specified layer.  When triggered,
	the animation modifies the alpha value of the widget's ``image`` member.
	"""

	def __init__(self, wm, layer, color=(0,0,0), duration=0.5, toalpha=128, fromalpha=0):
		self.widget = qxp.FillWidget(color)
		self.widget.image.set_alpha(fromalpha)
		wm.addwidget(layer, self.widget)
		qxp.LerpAnimation.__init__(self, duration, toalpha, fromalpha)


	def reset(self, duration=None, toalpha=None, fromalpha=None):
		"""Reset the animation.  This allows to re-use the animation with
		the same generated widget.
		"""

		if duration is None: duration = self.duration
		if toalpha is None: toalpha = self.dest
		if fromalpha is None: fromalpha = self.widget.image.get_alpha()
		self.widget.image.set_alpha(fromalpha)
		qxp.LerpAnimation.__init__(self, duration, toalpha, fromalpha)

	def timestep(self, step):
		self.widget.image.set_alpha(qxp.LerpAnimation.timestep(self, step))
		self.widget.markdirty()

