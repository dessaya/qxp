# This file is part of the qxp package.
# Author: Diego Essaya <diego@qb9.net>
# Copyright (c) 2007 QB9 Entertainment
# License: See COPYING

"""Some basic gui widgets."""

import pygame
from widget import Widget

__docformat__ = 'restructuredtext en'

class Button(Widget):
	"""Base button widget class."""

	def __init__(self, normalsurface, hoversurface=None, pressedsurface=None, func=None, funcargs=(), alpha=True, **kwargs):
		"""Create a Button instance.

		:Arguments:
			- `normalsurface`: pygame surface (or a path to a file) to be
			  used for the button.
			- `hoversurface`: pygame surface (or a path to a file) to be
			  used when the mouse cursor hovers over the button.
			- `pressedsurface`: pygame surface (or a path to a file) to be
			  used when the button is being pressed.
			- `func`: Arbitrary function to be called when the button is
			  clicked.
			- `funcargs`: Arbitrary list of arguments to be passed to `func`.
			- `alpha`: Determines whether to use ``convert()`` or
			  ``convert_alpha()`` after loading the image(s).
			- `kwargs`: Arbitrary modifications to `self.rect`.
		"""
		self.surfaces = {
			'normal': Widget.loadimage(normalsurface, alpha),
			'hover': Widget.loadimage(hoversurface, alpha),
			'pressed': Widget.loadimage(pressedsurface, alpha),
		}
		Widget.__init__(self, self.surfaces['normal'], **kwargs)
		self.func = func
		self.funcargs = funcargs
		self.state = 'normal'

	def updateimage(self):
		self.image = self.surfaces[self.state]
		if self.image is None:
			self.image = self.surfaces['normal']
		self.markdirty()

	def setstate(self, state):
		if state == self.state: return
		self.state = state
		self.updateimage()

	def mousemotion(self, pos):
		if self.state == 'pressed': return
		self.setstate(('normal', 'hover')[self.rect.collidepoint(pos)])

	def mousebuttondown(self, pos, button):
		if not self.rect.collidepoint(pos): return
		self.setstate('pressed')

	def mousebuttonup(self, pos, button):
		if self.state == 'pressed' and self.func and self.rect.collidepoint(pos):
			self.func(*self.funcargs)
		self.setstate('normal')
		self.mousemotion(pos)

class TextButton(Button):
	"""A Button widget that automatically renders some text."""

	def __init__(self, text, func=None, funcargs=(), font=None, normalcolor=(128,128,128), hovercolor=(255,255,255), pressedcolor=(255,128,0), **kwargs):
		if not font: font = pygame.font.Font(None, 24)
		normalsurface = font.render(text, 1, normalcolor)
		hoversurface = font.render(text, 1, hovercolor)
		pressedsurface = font.render(text, 1, pressedcolor)
		Button.__init__(self, normalsurface, hoversurface, pressedsurface, func, funcargs, **kwargs)

class CheckButton(Button):
	"""Two-state button widget."""

	def __init__(self, offsurface, onsurface, hoveroffsurface=None, hoveronsurface=None, pressedoffsurface=None, pressedonsurface=None, value=False, func=None, funcargs=(), alpha=True, **kwargs):
		self.offsurface = Widget.loadimage(offsurface, alpha)
		self.onsurface = Widget.loadimage(onsurface, alpha)
		self.hoveroffsurface = Widget.loadimage(hoveroffsurface, alpha)
		self.hoveronsurface = Widget.loadimage(hoveronsurface, alpha)
		self.pressedoffsurface = Widget.loadimage(pressedoffsurface, alpha)
		self.pressedonsurface = Widget.loadimage(pressedonsurface, alpha)
		self.value = value
		normalsurface = (self.offsurface, self.onsurface)[self.value]
		hoversurface = (self.hoveroffsurface, self.hoveronsurface)[self.value]
		Button.__init__(self, normalsurface, hoversurface, None, func, funcargs, **kwargs)

	def updateimage(self):
		n, h, p = zip(
			(self.offsurface, self.onsurface),
			(self.hoveroffsurface, self.hoveronsurface),
			(self.pressedoffsurface, self.pressedonsurface),
		)[self.value]
		self.surfaces = {'normal': n, 'hover': h, 'pressed': p}
		Button.updateimage(self)
	
	def mousebuttonup(self, pos, button):
		if self.state == 'pressed' and self.rect.collidepoint(pos):
			self.value = not self.value
		Button.mousebuttonup(self, pos, button)

class TextCheckButton(CheckButton):
	"""Ugly basic check button that shows a rectangle and some text."""

	def __init__(self, text, value=False, func=None, funcargs=(), font=None, textcolor=(128,128,128), hovertextcolor=(255,255,255), pressedtextcolor=(255,128,0), bordercolor=(128,128,128), offcolor=(0,0,0,0), oncolor=(255,128,0,255), boxsize=15, sep=5, **kwargs):
		if not font: font = pygame.font.Font(None, 24)
		
		def checkbox(on, state):
			tcolor = textcolor
			if state == 'hover':
				tcolor = hovertextcolor
			elif state == 'pressed':
				tcolor = pressedtextcolor
			textsurface = font.render(text, 1, tcolor)
			textrect = textsurface.get_rect()
			w = boxsize + sep + textrect.width + 2
			h = max(boxsize, textrect.height) + 2
			s = pygame.Surface((w, h)).convert_alpha()
			s.fill((0,0,0,0))
			srect = s.get_rect()
			textrect.midright = srect.midright
			s.blit(textsurface, textrect)
			fillcolor = (offcolor, oncolor)[on]
			boxrect = pygame.Rect(1, 1, boxsize, boxsize)
			pygame.draw.rect(s, fillcolor, boxrect)
			pygame.draw.rect(s, bordercolor, boxrect, 1)
			return s

		CheckButton.__init__(self,
			checkbox(False, 'normal'), checkbox(True, 'normal'),
			checkbox(False, 'hover'), checkbox(True, 'hover'),
			checkbox(False, 'pressed'), checkbox(True, 'pressed'),
			value, func, funcargs, **kwargs)

