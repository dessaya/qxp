# This file is part of the qxp package.
# Author: Diego Essaya <diego@qb9.net>
# Copyright (c) 2007 QB9 Entertainment
# License: See COPYING

"""The special FPS meter widget."""

import pygame
from widget import Widget
import qxp

__docformat__ = 'restructuredtext en'

class FPSWidget(Widget):
	"""A special widget that shows the FPS rate in the upper-right corner of
	the display.
	
	:Note: In order to force the rendering of the screen, this widget
		always marks itself as dirty.
	"""

	def __init__(self, font=None):
		Widget.__init__(self)
		self.font = font or pygame.font.Font(None, 24)
		self.last_update = float('-inf')

	def timestep(self, step):
		if self.wm.elapsedms - self.last_update > 500:
			self.image = self.font.render('%.1f' % (qxp.display._renderclock.get_fps()), 0, (255,255,255), (0,0,0))
			self.last_update = self.wm.elapsedms
			self.updaterect(self.image.get_rect(topright=qxp.display.get_rect().topright))
		else:
			self.markdirty()

