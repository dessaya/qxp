# This file is part of the qxp package.
# Author: Diego Essaya <diego@qb9.net>
# Copyright (c) 2007 QB9 Entertainment
# License: See COPYING

"""The image widget."""

import pygame
from widget import Widget

__docformat__ = 'restructuredtext en'

class ImageWidget(Widget):
	"""Basic widget that loads and shows a static image."""
	
	def __init__(self, image, alpha=False, **kwargs):
		"""Create an ImageWidget.
		
		:Arguments:
			- `image`: Can be either a path to an image file, or a pygame
			  surface.
			- `alpha`: Determines whether to use ``convert()`` or
			  ``convert_alpha()`` after loading the image.
			- `kwargs` -- Arbitrary modifications to `self.rect`.
		"""

		Widget.__init__(self, Widget.loadimage(image, alpha), **kwargs)

