# This file is part of the qxp package.
# Author: Diego Essaya <diego@qb9.net>
# Copyright (c) 2007 QB9 Entertainment
# License: See COPYING

"""The base widget class."""

import pygame
import qxp

__docformat__ = 'restructuredtext en'

class Widget(object):
	"""Base widget class.
	
	This is the base class used to implement all elements in qxp that can
	be drawn and/or that may receive events.

	This base class has no actual functionality; its callback methods should be
	overridden in the derived classes to provide the desired functionality.
	Just make sure to call `markdirty()` after a state change requiring
	a redraw.
	
	**Note**: Most callbacks are not defined in the Widget class (because they
	are not used, and to avoid the overhead of calling an empty function).  For
	the callbacks that are defined in the base `Widget` class (namely `added()`
	and `removed()`), you should manually call the base class method if
	you override it.  See `WidgetManager` for more details.

	Callbacks defined in the base `Widget` class:

	* `added(self, wm)`: Called after the widget is added to a WidgetManager.
	
	* `removed(self)`: Called after being removed from a WidgetManager.

	Callbacks not defined in the base `Widget` class:

	* ``mousebuttondown(self, pos, button)``: Called after a mouse button press event.
	
	* ``mousebuttonup(self, pos, button)``: Called after a mouse button release event.
	
	* ``mousemotion(self, pos)``: Called after a mouse move event.
	
	* ``keyup(self, key)``: Called after a keyboard button release event.
	
	* ``keydown(self, key)``: Called after a keyboard button press event.
	
	* ``timestep(self, step)``: Called after a timestep event.
		
	  The ``step`` argument is the time period, in seconds, that has passed since
	  the last call to ``timestep()``.  Note that the `WidgetManager.elapsedms`
	  variable contains the total elapsed time in milliseconds, since the start
	  of the main loop.
	
	* ``startloop(self)``: Called imediately after the WidgetManager's main
	  loop starts.
	
	* ``endloop(self)``: Called imediately before the WidgetManager's main
	  loop ends.

	:IVariables:
		- `rect`: ``pygame.Rect`` instance that determines the position and
		  size of the widget.
		- `image`: Reference to a pygame surface.  If assigned, it is used
		  in the default `render()` method to draw the widget.
		- `wm`: Reference to the WidgetManager containing this widget.  It
		  is automatically assigned in the `added()` callback.
	"""

	def __init__(self, image=None, rect=None, wm=None, **kwargs):
		"""Create a Widget instance.

		:Arguments:
			- `image`: Reference to a pygame surface.  If assigned, it is used
			  in the default `render()` method to draw the widget.
			- `rect`: ``pygame.Rect`` instance that determines the position and
			  size of the widget.  If ``image`` is not None, and ``rect``
			  is omitted, the ``rect`` value will be taken from
			  ``image.get_rect()``.  Otherwise, the reference to the rect
			  is stored, so you should make a copy if you will modify it later.
			- `wm`: Reference to the WidgetManager containing this widget.  It
			  is automatically assigned in the `added()` callback.
			- `kwargs`: You may add arbitrary assignments to any of the
			  ``Rect`` attributes, that will be applied to the rect
			  before storing it in `self.rect`.  For example:
			  ``Widget.__init__(rect=some_rect, center=(400,356))``
		"""

		self.wm = wm
		self.image = image
		self.rect = rect
		if image is not None and self.rect is None:
			self.rect = image.get_rect(**kwargs)
		elif self.rect:
			for k, v in kwargs.iteritems():
				self.rect.__setattr__(k, v)

	def added(self, wm, layer):
		"""Called after being added to a WidgetManager.
		
		This default callback calls `markdirty()` and assigns `self.wm`.
		"""

		self.markdirty()
		self.wm = wm

	def removed(self):
		"""Called after being removed from a WidgetManager.
		
		This default callback calls `markdirty()` and assigns
		`self.wm` to ``None``.
		"""
		self.markdirty()
		self.wm = None

	def render(self, surface, dirtyrect):
		"""Draw the part of the widget inside the dirtyrect, to the surface.

		If this method is overridden, make sure that only the area inside
		`dirtyrect` is modified. Also, note that this method may be called more
		than once for each render phase (to render different dirty rects inside
		the widget's rect), so it should not modify any state variables.
		
		This default callback blits `self.image` into `self.rect`, if both
		are assigned.
		
		:Precondition: `dirtyrect` is completely contained into `self.rect`.
		"""

		if self.image is None or self.rect is None: return
		surface.set_clip(dirtyrect)
		surface.blit(self.image, self.rect)
		surface.set_clip()

	def updaterect(self, rect, **kwargs):
		"""Replace self.rect and mark both old and new as dirty.
		
		The reference to the rect is stored, so you should make a copy if you
		will modify it later.  Use the keyword args to apply valid
		modifications to the rect.  For example::
		
			myrect = Rect(0, 0, 10, 10)
			widget1.updaterect(Rect(myrect), topleft=(30, 25))
			widget2.updaterect(Rect(myrect), center=(80, 10))
		"""

		self.markdirty()
		for k, v in kwargs.iteritems():
			rect.__setattr__(k, v)
		self.rect = rect
		self.markdirty()

	def modifyrect(self, **kwargs):
		"""Apply a transformation to self.rect and mark both old and new as
		dirty.
		
		This is equivalent to ``updaterect(pygame.rect(self.rect), **kwargs)``.
		"""

		self.updaterect(pygame.Rect(self.rect), **kwargs)

	def markdirty(self):
		"""Mark self.rect as dirty, to ensure that `self.render()` will be
		called on the next render phase.

		This is equivalent to ``qxp.display.markdirty(self.rect)``.
		"""

		if self.rect:
			qxp.display.markdirty(self.rect)

	def movetolayer(self, layer):
		"""Move the widget to another layer in the same WidgetManager,
		and mark as dirty.
		
		:Note: As a side effect, the `removed()` and `added()` callbacks will
			be executed.
		"""
		# removewidget() unassigns self.wm
		wm = self.wm
		self.wm.removewidget(self)
		wm.addwidget(layer, self)

	def RectAnimation(self, *args, **kwargs):
		"""Shortcut to create an `animation.RectAnimation` instance."""
		return qxp.RectAnimation(self, *args, **kwargs)

	@staticmethod
	def loadimage(image, alpha=False):
		"""Utility function that calls pygame.image.load() if `image` is
		a path.

		:Arguments:
			- `image`: Can be either a path to an image file, or a pygame
			  surface.
			- `alpha`: Determines whether to use ``convert()`` or
			  ``convert_alpha()`` after loading the image.

		:Returns: A pygame surface, or None.
		"""

		if image is None:
			return None
		if not isinstance(image, str):
			return image
		s = pygame.image.load(image)
		if alpha:
			return s.convert_alpha()
		else:
			return s.convert()
		
