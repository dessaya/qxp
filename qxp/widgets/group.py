# This file is part of the qxp package.
# Author: Diego Essaya <diego@qb9.net>
# Copyright (c) 2007 QB9 Entertainment
# License: See COPYING

"""The special group widget."""

import pygame
from widget import Widget

__docformat__ = 'restructuredtext en'

class GroupWidget(Widget, list):
	"""Special widget useful for grouping other widgets together and moving
	them as a whole.
	
	Grouped widgets do not work as a tree; ie, nested groups are not allowed
	(yet).  Instead, a group is just a widget that references other widgets,
	but these other widgets are normally contained in the parent
	`WidgetManager`, so their event handlers are called in the same way.

	Use `addwidget()` to add widgets to the group **before** adding the group
	to a layer.  All widgets of a group must be assigned to the same layer.

	`self.rect` will be automatically calculated after adding widgets, so
	that it will contain all of them.
	"""

	def addwidget(self, *widgets):
		"""Add one or more widgets to the group.
		
		You must call it before adding the group to a layer.
		"""

		for widget in widgets:
			self.append(widget)
		self.calcrect()

	def removewidget(self, widget):
		"""Remove a widget from the group, and also remove it from the
		`WidgetManager`."""
		self.wm.removewidget(widget)
		self.remove(widget)
		self.calcrect()

	def added(self, wm, layer):
		"""Called after being added to a WidgetManager.
		
		This default callback automatically adds the contained widgets to
		the WidgetManager in the given layer.
		"""

		Widget.added(self, wm, layer)
		for widget in self:
			self.wm.addwidget(layer, widget)

	def removed(self):
		"""Called after being removed from a `WidgetManager`.
		
		This default callback automatically removes the contained widgets
		from the `WidgetManager`.
		"""

		for widget in self:
			self.wm.removewidget(widget)
		Widget.removed(self)

	def calcrect(self):
		"""Calculate `self.rect` to contain all rects.

		This method is automatically called after adding and removing widgets.
		"""

		self.markdirty()
		self.rect = None
		for widget in self:
			if not widget.rect: continue
			if self.rect is None:
				self.rect = pygame.Rect(widget.rect)
			else:
				self.rect.union_ip(widget.rect)
		self.markdirty()

	def offset(self, x, y):
		"""Move all widgets in the group by an offset."""
		self.markdirty()
		self.rect.move_ip(x, y)
		for widget in self:
			widget.rect.move_ip(x, y)
		self.markdirty()

	def updaterect(self, rect, **kwargs):
		"""Replace self.rect and mark both as dirty.
		
		The new rect should have the same size as the current `self.rect`.
		Make a copy of rect if you will modify it later.
		"""
		if not self.rect: raise ValueError('self.rect has not been assigned.')
		for k, v in kwargs.iteritems():
			rect.__setattr__(k, v)
		x = rect.left - self.rect.left
		y = rect.top - self.rect.top
		self.offset(x, y)

