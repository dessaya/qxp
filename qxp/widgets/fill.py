# This file is part of the qxp package.
# Author: Diego Essaya <diego@qb9.net>
# Copyright (c) 2007 QB9 Entertainment
# License: See COPYING

"""Fill a rect with a plain color."""

import pygame
from widget import Widget
import qxp

__docformat__ = 'restructuredtext en'

class FillWidget(Widget):
	"""Basic widget that fills a rectangular area with a plain color.
	
	It uses the `Widget.image` member to store the surface filled with the
	plain color.  This allows to apply an alpha value, for example as in
	the `FadeAnimation` class.
	"""
	
	def __init__(self, color=(0,0,0), rect=None):
		rect = rect or qxp.display.get_rect()
		image = pygame.Surface(rect.size)
		image.fill(color, image.get_rect())
		Widget.__init__(self, image, rect)

