# This file is part of the qxp package.
# Author: Diego Essaya <diego@qb9.net>
# Copyright (c) 2007 QB9 Entertainment
# License: See COPYING

"""The label widget."""

import pygame
from widget import Widget

__docformat__ = 'restructuredtext en'

class Label(Widget):
	"""Static text widget.
	
	:IVariables:
		- `text`: Read-write property.
	"""

	def __init__(self, text, font=None, color=(255,255,255), **kwargs):
		self._text = text
		self.color = color
		self.font = font or pygame.font.Font(None, 24)
		Widget.__init__(self, self.getsurface(), **kwargs)

	def gettext(self):
		return self._text
	
	def settext(self, text):
		self._text = text
		self.image = self.getsurface()
		self.updaterect(self.image.get_rect(topleft=self.rect.topleft))

	def getsurface(self):
		return self.font.render(self._text, 1, self.color)

	text = property(gettext, settext)

