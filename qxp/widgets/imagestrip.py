# This file is part of the qxp package.
# Author: Diego Essaya <diego@qb9.net>
# Copyright (c) 2007 QB9 Entertainment
# License: See COPYING

"""The image strip widget."""

import pygame
from image import ImageWidget

__docformat__ = 'restructuredtext en'

class ImageStrip(ImageWidget):
	"""A widget that allows to create an animated 'movie' based on
	a single source image.  This image is expected to have the complete
	set of the animation frames arranged in a grid layout.
	
	:IVariables:
		- `cols`: Number of columns in the strip.
		- `rows`: Number of rows in the strip.
		- `frame`: Current frame.  Use `setframe()` to change its value.
		- `frames`: Total number of frames.
		- `fps`: Frames per second. Use `setfps()` to change its value.
		- `loop`: True if the animation loops indefinitely.
	"""
	
	def __init__(self, image, cols=1, rows=1, frames=None, fps=0, loop=True, alpha=False, **kwargs):
		"""Create an ImageStrip.
		
		:Arguments:
			- `image`: Can be either a path to an image file, or a pygame
			  surface.
			- `cols`: Number of columns in the source image.
			- `rows`: Number of rows in the source image.
			- `frames`: Total number of frames in the strip.  If omitted, it
			  will be calculated as ``cols * rows``.
			- `fps`: FPS rate of the animation.  If omitted or set to 0, 
			  the animation will not play automatically, and you will have
			  to use `nextframe()` or `setframe()` to animate manually.
			- `loop`: If set to False, the animation will play once, and
			  then the widget will be automatically removed from the
			  widget manager.  If you need to change this behavior you must
			  override the `lastframe()` method.
			- `alpha`: Determines whether to use ``convert()`` or
			  ``convert_alpha()`` after loading the image.
			- `kwargs`: Arbitrary modifications to `self.rect`.
		"""

		ImageWidget.__init__(self, image, alpha)
		self.frame = 0
		self.cols = cols
		self.rows = rows
		self.frames = frames or (cols * rows)
		self.fps = 0
		self.setfps(fps)
		self.loop = loop
		self._t = 0
		
		self.rect.width /= cols
		self.rect.height /= rows
		self.modifyrect(**kwargs)
		
		self._strip = self.image
		self.image = self._getsub()

	def setfps(self, fps):
		"""Change the value of the `fps` instance variable."""
		self.fps = float(fps)
		self._fpsstep = int(1000 / self.fps)

	def _getsub(self):
		"""Return the subsurface that corresponds to the current frame
		in the source image grid.
		"""

		col, row = self.frame % self.cols, self.frame / self.cols
		rect = pygame.Rect(self.rect)
		rect.topleft = self.rect.width * col, self.rect.height * row
		return self._strip.subsurface(rect)

	def setframe(self, frame):
		"""Switch to an arbitrary frame number,"""
		self.frame = frame
		self.image = self._getsub()
		self.markdirty()

	def lastframe(self):
		"""Called when the last frame is reached.
		
		If `loop` is False, the widget will be removed from the widget
		manager.  Override this method to change this behavior.

		:Returns: True if the widget has been removed from the widget manager.
		"""

		if self.loop:
			self.setframe(0)
			return False
		self.wm.removewidget(self)
		return True

	def nextframe(self):
		"""Switch to the next frame in the sequence.

		If the current frame is the last frame, `lastframe()` is called
		instead.

		:Returns: True if the widget has been removed from the widget manager.
		"""

		if self.frame == self.frames - 1:
			return self.lastframe()
		self.setframe(self.frame + 1)
		return False

	def timestep(self, step):
		if not self.fps:
			return
		self._t += int(step * 1000)
		while self._t > self._fpsstep:
			if self.nextframe():
				break
			self._t -= self._fpsstep

