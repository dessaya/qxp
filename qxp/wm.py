# This file is part of the qxp package.
# Author: Diego Essaya <diego@qb9.net>
# Copyright (c) 2007 QB9 Entertainment
# License: See COPYING

"""WidgetManager module -- the brain of qxp."""

import pygame
import sys
import itertools

from animation import AnimationManager
import events
import display
import cursor
from layer import Layer

__docformat__ = 'restructuredtext en'

class EndLoopException(Exception):
	def __init__(self, retval=None):
		self.retval = retval

class WidgetManager(object):
	"""Widgets, events and rendering manager.
	
	This class can be thought as the 'brain' of the qxp system, as it
	implements some of the most common tasks needed in every pygame
	application.

	Features:
	
	* Implements a main `loop`.
	* Manages a set of widgets (see the `Widget` class), ordered in layers.
	* Implements a wrapper for the pygame event subsystem, allowing to define
	  callback functions for any pygame event.

	The event subsystem is implemented in several steps:
	
	1. The main loop receives an event from the pygame event queue.
	
	2. If the event type is present in the `handlers` dictionary, the
	   appropriate handler is called.

	   You can add a handler for any pygame event using the `sethandler()`
	   method.  If you want to ignore a certain event type you can use
	   `sethandler(event, None)`.

	3. Some of the default handlers extract the useful data from the pygame
	   event instance and then broadcast the event to the objects that
	   define a handler for that specific event.  For example, the
	   `evt_mousemotion()` handler will call ``obj.mousemotion(pos)``
	   for all objects that define the ``mousemotion()`` method.
	
	   Here is the default list of handled events, with their respective
	   handler function and broadcasted function and arguments:

	   .. parsed-literal::

	   	======================  ===============  =========================
	   	Event                   WM handler       Broadcasted function call
	   	======================  ===============  =========================
	   	pygame.QUIT             `evt_quit`         -
	   	events.timerevent       `evt_timer`        timestep(step)
	   	pygame.KEYDOWN          `evt_keydown`      keydown(key)
	   	pygame.KEYUP            `evt_keyup`        keyup(key)
	   	pygame.MOUSEBUTTONUP    `evt_mbup`         mousebuttonup(pos, button)
	   	pygame.MOUSEBUTTONDOWN  `evt_mbdown`       mousebuttondown(pos, button)
	   	pygame.MOUSEMOTION      `evt_mousemotion`  mousemotion(pos)
		(Before the main loop starts)            startloop()
		(After finishing the main loop)          endloop()
	   	======================  ===============  =========================

	   See `broadcast` for more details.

	This class also implements a nifty shortcut method to handle specific
	key press events.  The following handlers are configured by default:

	* Quit the application by pressing 'q'.  You can cancel this behavior with
	  `set_keydown_handler(pygame.K_q, None)`.
	* Toggle fulscreen mode by pressing 'f'.  You can cancel this behavior with
	  `set_keydown_handler(pygame.K_f, None)`.

	:IVariables:
		- `layers`: The list of layers, initially empty. Use `newlayer()` to create a layer.
		- `elapsedms`: The elapsed time, in ms.  Use this instead of `pygame.time.get_ticks()` if
		  you don't want to take into account the periods in which the execution was paused.
		- `handlers`: ``event -> func`` dictionary. Use `sethandler()` to add or remove handlers.
		- `keydown_handlers`: ``key -> (func, *args)`` dictionary, for keydown events.
		  Use `set_keydown_handler()` to add or remove handlers.
		- `keyup_handlers`: ``key -> (func, *args)`` dictionary, for keyup events.
		  Use `set_keyup_handler()` to add or remove handlers.
		- `animations`: An `AnimationManager` instance.
	"""

	def __init__(self):
		self.layers = []
		self.elapsedms = 0
		self.timer = pygame.time.Clock()
		self.fps = 0
		self.fpsstep = 0
		self.timerstep = 0
		self.animations = AnimationManager()
		self.handlers = {
			pygame.QUIT: self.evt_quit,
			events.timerevent: self.evt_timer,
			pygame.KEYDOWN: self.evt_keydown,
			pygame.KEYUP: self.evt_keyup,
			pygame.MOUSEBUTTONUP: self.evt_mbup,
			pygame.MOUSEBUTTONDOWN: self.evt_mbdown,
			pygame.MOUSEMOTION: self.evt_mousemotion,
		}
		self.keydown_handlers = {
			pygame.K_f: (display.toggle_fullscreen, ()),
			pygame.K_q: (sys.exit, ()),
		}
		self.keyup_handlers = {}

	def broadcast(self, callback, *args):
		"""Broadcast a function call.

		This method calls ``obj.callback(*args)`` for all objects that
		define the given callback function.

		The objects that are inspected are:

		* ``self`` (the widget manager).
		* `self.animations` (the WM's animation manager), which defines
		  only the ``timestep()`` callback.
		* The `cursor` module, which defines the ``mousemotion()`` callback
		  in order to update the cursor widget's position.
		* The ``cursor.widget``.
		* All widgets contained in the widget manager.

		:Arguments:
			- `callback`: The name of the function to call.
			- `args`: Any number of arbitrary arguments.
		"""

		widgets = iter(self)
		listeners = itertools.chain(
			(self, self.animations, cursor, cursor.widget), widgets,
		)
		for f in itertools.ifilter(
			None, ( getattr(obj, callback, None) for obj in listeners ),
		):
			f(*args),

	def sethandler(self, event, handler):
		"""Add or remove an event handler.
		
		You should call it before executing the main loop. Otherwise you will need to call 
		`pygame.event.set_allowed(self.handlers.keys())` to make sure that all handled events
		are allowed by pygame.

		:Arguments:
			- `event`: A valid pygame event type.
			- `handler`: A function handler that will receive one argument:
			  the pygame event.  Set ``handler=None`` to completely ignore the
			  event.
		"""

		if handler is not None:
			self.handlers[event] = handler
		else:
			if event in self.handlers:
				del self.handlers[event]

	def set_keydown_handler(self, key, handler, *args):
		"""Add or remove a handler for a specific key press event.
		
		:Arguments:
			- `key`: The pygame identifier for the key.
			- `handler`: A reference to a function callback.  Set it to
			  None to remove a previously defined handler.
			- `args`: The arbitrary list of arguments that will be passed
			  to the function.
		"""

		if handler is not None:
			self.keydown_handlers[key] = (handler, args)
		else:
			if key in self.keydown_handlers:
				del self.keydown_handlers[key]

	def set_keyup_handler(self, key, handler, *args):
		"""Add or remove a handler for a specific key release event.
		
		:Arguments:
			- `key`: The pygame identifier for the key.
			- `handler`: A reference to a function callback.  Set it to
			  None to remove a previously defined handler.
			- `args`: The arbitrary list of arguments that will be passed
			  to the function.
		"""

		if handler is not None:
			self.keyup_handlers[key] = (handler, args)
		else:
			if key in self.keyup_handlers:
				del self.keyup_handlers[key]

	def newlayer(self, name):
		"""Create a new layer and return it.
		
		Layers are ordered: the last created layer is drawn over the
		prevoius ones.  Normally it is more convenient to use `createlayers()`
		to create several layers at once.

		:Arguments:
			- `name`: An arbitrary name for the layer.
		"""

		layer = Layer(self, name)
		self.layers.append(layer)
		return layer

	def createlayers(self, *names):
		"""Shortcut for `newlayer()` to crate many layers at once."""
		for name in names:
			self.newlayer(name)

	def getlayer(self, layer):
		"""Return a reference to a layer.

		The single argument can be a reference to a layer (which will be
		returned), an integer (the index of the layer) or a string
		(the name of the layer).
		"""

		if isinstance(layer, Layer):
			return layer
		if isinstance(layer, int):
			return self.layers[layer]
		if isinstance(layer, str):
			for l in self.layers:
				if l.name == layer:
					return l
			raise ValueError('layer not found.')
		raise ValueError('layer must be of type Layer, int or str.')

	def addwidget(self, layer, *widgets):
		"""Add one or more widgets to a layer.
		
		This is equivalent to ``getlayer(layer).append(widget)``.  After this
		call, the `Widget.added()` callback will be executed.
		"""
		layer = self.getlayer(layer)
		for widget in widgets:
			layer.append(widget)

	def get_layer_containing_widget(self, widget):
		"""Return a reference to the layer that contains the widget, or None
		if not found.
		"""

		for layer in self.layers:
			if widget in layer:
				return layer
		return None

	def removewidget(self, *widgets):
		"""Remove the widget from its layer, and thus from the widget manager.
		
		After this call, the `Widget.removed()` callback will be executed.
		"""

		for widget in widgets:
			layer = self.get_layer_containing_widget(widget)
			if layer is None:
				raise ValueError('widget not found.')
			layer.remove(widget)

	def halt(self, retval=None):
		"""Finish the execution of the main loop.
		
		:Arguments:
			- `retval`: An arbitrary value that will be returned by `loop()`.
		"""
		
		raise EndLoopException(retval)

	def __iter__(self):
		"""Return an iterator for all the contained widgets. This allows
		to use the ``for widget in self`` idiom.
		"""
		
		# Take a snapshot of the current set of widgets, to avoid problems
		# if widgets get added or removed while iterating.
		return tuple(itertools.chain(*self.layers)).__iter__()
	
	def render(self, surface, dirtyrect=None):
		"""Render all widgets, in order, to the given surface.
		
		If dirtyrect is None, the whole surface will be rendered.
		
		:Note: This method is automatically called from the main `loop`.
		"""

		if dirtyrect is None:
			dirtyrect = surface.get_rect()
		for widget in self:
			if not widget.rect: continue
			cliprect = dirtyrect.clip(widget.rect)
			if not cliprect: continue
			widget.render(surface, cliprect)

	def rendertoscreen(self):
		"""Render all dirty rects and call `qxp.display.update()`.
		
		:Note: This method is automatically called from the main `loop`.
		"""

		for rect in display.dirtyrects:
			self.render(display.surface, rect)
		display.update()
	
	def block(self):
		"""Block all events. Calls ``pygame.event.set_allowed(None)``."""
		pygame.event.set_allowed(None)

	def unblock(self):
		"""Unblock all handled events."""
		self.timer.tick()
		pygame.event.set_allowed(None)
		pygame.event.set_allowed(self.handlers.keys())
		display.markdirty()

	def nestwm(self, wm, fps=None):
		"""Execute another WidgetManager's main loop, and return its return value.
		
		This method pauses the execution of the main loop, executes the main loop of the
		other WidgetManager until it finishes and then resumes execution.
		
		It is equivalent to the following calls::

			self.block()
			retval = wm.loop()
			self.unblock()
		
		:Arguments:
			- `wm`: A `WidgetManager` instance.
			- `fps`: FPS value to be passed to ``wm``'s `loop`. If omitted, the same
			  fps rate used for this WidgetManager will be passed.
		"""

		self.block()
		retval = wm.loop(fps or self.fps)
		self.unblock()
		return retval

	def evt_mbup(self, event):
		"""Handler for the ``pygame.MOUSEBUTTONUP`` event.
		
		This default handler calls the ``mousebuttonup()`` callback for all
		objects that define it.  See `WidgetManager` for more details.
		"""

		self.broadcast('mousebuttonup', event.pos, event.button)

	def evt_mbdown(self, event):
		"""Handler for the ``pygame.MOUSEBUTTONDOWN`` event.
		
		This default handler calls the ``mousebuttondown()`` callback for all
		objects that define it.  See `WidgetManager` for more details.
		"""

		self.broadcast('mousebuttondown', event.pos, event.button)

	def evt_mousemotion(self, event):
		"""Handler for the ``pygame.MOUSEMOTION`` event.
		
		This default handler calls the ``mousemotion()`` callback for all
		objects that define it.  See `WidgetManager` for more details.
		"""

		self.broadcast('mousemotion', event.pos)
	
	def evt_keydown(self, event):
		"""Handler for the ``pygame.KEYDOWN`` event.
		
		This default handler challs the appropriate handler
		installed in `keydown_handlers`, if present, and then calls
		the ``keydown()`` callback for all
		objects that define it.  See `WidgetManager` for more details.
		"""

		if event.key in self.keydown_handlers:
			handler, args = self.keydown_handlers[event.key]
			handler(*args)
		self.broadcast('keydown', event.key)

	def evt_keyup(self, event):
		"""Handler for the ``pygame.KEYUP`` event.
		
		This default handler challs the appropriate handler
		installed in `keyup_handlers`, if present, and then calls
		the ``keyup()`` callback for all
		objects that define it.  See `WidgetManager` for more details.
		"""

		if event.key in self.keyup_handlers:
			handler, args = self.keyup_handlers[event.key]
			handler(*args)
		self.broadcast('keyup', event.key)
	
	def evt_quit(self, event):
		"""Handler for the ``pygame.QUIT`` event.
		
		This default handler simply calls ``sys.exit()``.
		"""

		sys.exit()

	def evt_timer(self, event):
		"""Handler for the special ``events.timerevent`` event.
		
		This default handler calls the ``timestep()`` callback for all
		objects that define it, and then installs the next pygame timer,
		in order to maintain the desired FPS.
		See `WidgetManager` for more details.
		"""

		stepms = self.timer.tick()
		self.elapsedms += stepms
		self.broadcast('timestep', stepms / 1000.0)
		
		self.timerstep = max(1, self.timerstep + (self.fpsstep - stepms) / 2)
		pygame.event.clear(events.timerevent)
		pygame.time.set_timer(events.timerevent, self.timerstep)

	def loop(self, fps=50):
		"""Main loop.

		This is the ultimate pygame main loop, polished after years of
		gathering knowledge.

		Ok, it is not *the ultimate*, but it's pretty good. Really.
		
		Features:

		* Keeps a maximum FPS rate, to avoid consuming CPU.
		* Calls `rendertoscreen()` only when needed, based on the list of
		  dirty rects.
		* Calls event handlers based on the `handlers` dictionary.

		:Arguments:
			- `fps`: Maximum desired FPS rate.
		"""

		self.broadcast('startloop')

		retval = None
		self.unblock()
		self.fps = float(fps)
		self.fpsstep = int(1000 / self.fps)
		self.timerstep = self.fpsstep
		pygame.time.set_timer(events.timerevent, self.timerstep)
		lastrender = 0

		try:
			while True:
				# Render if necessary
				now = pygame.time.get_ticks()
				if display.dirtyrects and (
					now - lastrender > self.fpsstep or \
					not pygame.event.peek(self.handlers.keys())
				):
					lastrender = now
					self.rendertoscreen()
				
				# Wait for an event and process it
				event = pygame.event.wait()
				handler = self.handlers.get(event.type, None)
				if handler is not None:
					handler(event)
		
		except EndLoopException, e:
			retval = e.retval

		self.broadcast('endloop')

		return retval

