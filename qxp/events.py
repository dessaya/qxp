# This file is part of the qxp package.
# Author: Diego Essaya <diego@qb9.net>
# Copyright (c) 2007 QB9 Entertainment
# License: See COPYING

"""Declaration of internally used events.

:Variables:
	- `timerevent`: Used in the `WidgetManager` class to keep track of time.
	- `USEREVENT`: Replacement for ``pygame.USEREVENT``.
"""

import pygame

__docformat__ = 'restructuredtext en'

def __newevent():
	i = pygame.USEREVENT
	while True:
		yield i
		i += 1
_newevent = __newevent().next
	
timerevent = _newevent()
USEREVENT = _newevent()

# Replace pygame.USEREVENT:
pygame.USEREVENT = USEREVENT

