# This file is part of the qxp package.
# Author: Diego Essaya <diego@qb9.net>
# Copyright (c) 2007 QB9 Entertainment
# License: See COPYING

"""Methods for displaying a full color mouse cursor of any size.

This is a wrapper for the pygame cursor methods, which constrain the cursor
to be black and white.  The idea here is that you can assign any Widget
instance (typically `ImageWidget`) to be the mouse cursor.  This allows to
have a cursor with any arbitrary size, in full color, and eventually to
have animation and respond to events.

:Variables:
	- `widget`: The `Widget` instance used to render the cursor.
"""

import pygame
from widgets import Widget, ImageWidget
import qxp

__docformat__ = 'restructuredtext en'

widget = None

def set(cursor):
	"""Set the cursor appearance.
	
	:Arguments:
		- `cursor`: Can be either ``None`` (to use the OS default cursor),
		  a path to an image file, a pygame surface or a `Widget` instance.
	"""

	global widget
	if widget is not None:
		widget.removed()
		widget = None
	if cursor is not None:
		if isinstance(cursor, Widget):
			widget = cursor
			widget.added(wm=None, layer=None)
		else:
			widget = ImageWidget(cursor, alpha=True, topleft=pygame.mouse.get_pos())
	pygame.mouse.set_visible(cursor is None)

def render():
	"""Render the cursor, if necessary.
	
	Automatically called from `display.update()`.
	"""

	global widget
	if widget is None: return
	for rect in qxp.display.dirtyrects:
		cliprect = rect.clip(widget.rect)
		if not cliprect: continue
		widget.render(qxp.display.surface, cliprect)

def mousemotion(pos):
	"""Update the cursor widget with the current mouse position."""
	if widget is None: return
	widget.modifyrect(topleft=pos)

