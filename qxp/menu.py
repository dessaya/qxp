# This file is part of the qxp package.
# Author: Diego Essaya <diego@qb9.net>
# Copyright (c) 2007 QB9 Entertainment
# License: See COPYING

"""Classes for creating a menu screen."""

import pygame
from wm import WidgetManager
from widgets import ImageWidget

__docformat__ = 'restructuredtext en'

class Menu(WidgetManager):
	"""Generic class for creating a menu screen."""
	def __init__(self, bg):
		WidgetManager.__init__(self)
		layer = self.newlayer('bg')
		self.addwidget(layer, ImageWidget(bg))
		for key in pygame.K_ESCAPE, pygame.K_q:
			self.set_keydown_handler(key, self.halt, ('esc',))

