# This file is part of the qxp package.
# Author: Diego Essaya <diego@qb9.net>
# Copyright (c) 2007 QB9 Entertainment
# License: See COPYING

"""Layer subsystem."""

import pygame

__docformat__ = 'restructuredtext en'

class Layer(list):
	"""A rendering layer containing a group of widgets. Use
	`WidgetManager.newlayer()` to create a layer, or more preferably,
	`WidgetManager.createlayers()` to create several layers at once.
	"""

	def __init__(self, wm, name):
		list.__init__(self)
		self.name = name
		self.wm = wm

	def __repr__(self):
		return "<Layer '%s'>" % self.name

	def append(self, widget):
		"""Add a widget to the layer.
		
		This is equivalent to ``WidgetManager.addwidget(layer, widget)``.
		The `Widget.added()` callback will be called after appending the
		widget.
		"""

		list.append(self, widget)
		widget.added(self.wm, self)

	def remove(self, widget):
		"""Remove a widget from the layer, and thus from the widget manager.
		
		This is equivalent to ``WidgetManager.removewidget(widget)``.
		The `Widget.removed()` callback will be called after removing the
		widget.
		"""

		list.remove(self, widget)
		widget.removed()

