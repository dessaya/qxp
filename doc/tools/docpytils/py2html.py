#!/usr/bin/env python

"""Usage: generate-html.py [-d dir] FILES..."""

import sys, os
import docpytils
from docutils.core import publish_cmdline, default_description
from string import Template

template = Template(file(
	os.path.join(
		os.path.dirname(__file__),
		'template.txt'
	)
).read())

def do_pycode(input):
	global template
	title = os.path.basename(input)
	title = title.replace('tut', 'Tutorial #')
	title = title.replace('-', ': ', 1)
	title = title.replace('-', ' ')
	title = title.replace('.py', '')
	underline = '=' * len(title)
	return template.substitute(title=title, underline=underline, input=os.path.abspath(input))

tmp_files = []

def process_argv():
	global tmp_files
	argv = []

	for i, arg in enumerate(sys.argv[1:]):
		if arg.endswith('.py'):
			tmpfile = os.path.join('/tmp', os.path.basename(arg))
			tmpfile = tmpfile.replace('.py', '.txt')
			file(tmpfile, 'w').write(do_pycode(arg))
			tmp_files.append(tmpfile)
			arg = tmpfile
		argv.append(arg)

	return argv

print sys.argv[-2]

description = ('Generates (X)HTML documents from standalone reStructuredText '
               'sources, or python source files.  ' + default_description)

argv = process_argv()

publish_cmdline(writer_name='html', description=description, argv=argv)

for f in tmp_files:
	os.remove(f)

