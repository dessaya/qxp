#!/usr/bin/env python
#
# qxp tutorial #7: Animations
# ===========================
#
# qxp includes a set of classes that allow to implement smooth variations
# to widget properties over time.  In other words, animations =).
#
# This set of classes is strongly inspired in the Panda3D interval system,
# so if you are familiar with Panda3D, you will find similar concepts here.
# However, the qxp Animations package is not feature complete yet, so don't
# expect to find all the bells and whistles...
#
# In this tutorial we will take the tutorial #5 (mouse input) as a base,
# and modify it to make it use the qxp animations system.

import pygame
import qxp

# You missed the Thingy?  Don't worry, it's back!
class Thingy(qxp.FillWidget):
	def __init__(self):
		rect = pygame.Rect(0,0,10,10)
		rect.center = qxp.display.get_rect().center
		qxp.FillWidget.__init__(self, (255,128,0), rect)

		# A reference to an Animation instance.  Initially the thingy
		# stands still, so we set it to None.
		self.animation = None

	def mousebuttondown(self, pos, button):
		# If the thingy is moving, remove the animation from the animation
		# manager.
		if self.animation is not None and not self.animation.finished:
			self.wm.animations.remove(self.animation)

		# Side note:  The self.wm member is the third instance variable
		# inherited from the Widget class (remember, the other two are rect
		# and image).  It is a reference to the WidgetManager that contains
		# the widget.  Initially it is set to None, and is automatically
		# bound to the parent WidgetManager when added to it (ie, in the
		# WidgetManager.addwidget() method).

		# Compute the destination rect
		destrect = pygame.Rect(self.rect)
		destrect.center = pos

		# Create a RectAnimation instance.  The RectAnimation modifies the
		# rect member of a widget over time.  This allows to move the
		# widget, or even resize it.
		#
		# We set its duration to 1 second, the destination rect (self.rect
		# is used for the source rect by default), and we set the animation
		# to smoothly 'brake' before reaching the destination.
		self.animation = self.RectAnimation(1.0, destrect, easeout=True)

		# Add the animation to the animation manager.
		self.wm.animations.add(self.animation)

class Tutorial7(qxp.WidgetManager):
	def __init__(self):
		qxp.WidgetManager.__init__(self)
		self.createlayers('bg', 'fg')

		bg = qxp.FillWidget(color=(128,64,0))
		self.addwidget('bg', bg)

		label = qxp.Label('Click anywhere on the screen.')
		self.addwidget('fg', label)

		thingy = Thingy()
		self.addwidget('fg', thingy)

if __name__ == '__main__':
	pygame.init()
	qxp.display.init('qxp tutorial #7', (500, 500))
	Tutorial7().loop()

# Excercise: play with the duration and easein / easeout arguments of the
# RectAnimation.
#
# In the next tutorial we will learn to code our own custom Animations to
# modify any arbitrary property of a widget (or anything else).

