#!/usr/bin/env python
#
# qxp tutorial #9: Image strips
# =============================
#
# The qxp.ImageStrip widget allows to create an animated 'movie' based on
# a single source image.  This image is expected to have the complete set
# of the animation frames arranged in an 'n x m' grid layout.
#
# In this tutorial we use this feature to show a cool explosion when the
# user clicks on the screen.

import pygame
import qxp
import random

class Tutorial9(qxp.WidgetManager):
	def __init__(self):
		qxp.WidgetManager.__init__(self)
		self.createlayers('bg', 'fg')
		self.addwidget('bg', qxp.FillWidget((0, 0, 0)))
		
		# Since we don't want to load the source image from disk every
		# time we show a new explosion, we load it once here and store
		# it for later.
		self.strip = pygame.image.load('explosion.png').convert_alpha()
		
		self.addwidget('fg', qxp.Label(
			'Click anywhere on the screen.'
		))
		self.addwidget('fg', qxp.FPSWidget())

	def new_explosion(self, pos=None):
		# Create an explosion on either a fixed or random position.
		if pos is None:
			w, h = qxp.display.surface.get_size()
			pos = random.randint(0, w), random.randint(0, h)
		
		# Here we create our ImageStrip instance.  The source image
		# (explosion.png) has a total of 17 frames arranged in a 17x1 grid.
		# We set the animation speed to 24 frames / second.
		# We also set loop=False; this will make the widget automatically
		# disappear when the explosion finishes.  (And since we don't store
		# a reference to it, the garbage collector will destroy it).
		explosion = qxp.ImageStrip(
			self.strip, cols=17, fps=24, loop=False, center=pos
		)
		self.addwidget('fg', explosion)

	def mousebuttondown(self, pos, button):
		self.new_explosion(pos)
		# Tip: try the mouse wheel!

	# Uncomment this to wreak havoc!!
	#def timestep(self, step):
	#	self.new_explosion()

if __name__ == '__main__':
	pygame.init()
	qxp.display.init('qxp tutorial #9', (500, 500))
	Tutorial9().loop()

# Excercise: Have you tried uncommenting the timestep() method?  Modify the
# code to implement a flamethrower effect, that is on whenever the button
# is pressed, and switches off when the button is released.

