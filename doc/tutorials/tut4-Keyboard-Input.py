#!/usr/bin/env python
#
# qxp tutorial #4: Keyboard input
# ===============================
#
# In this tutorial we will create a widget derived from FillWidget and
# add some behavior to it.

import pygame
import qxp

# Declare the Thingy class.  The Thingy is a... thing that will show on
# screen and we will be able to move it around using the keyboard arrows.
# We are deriving it from the FillWidget class, so it will be rendered as a
# solid rectangle.
class Thingy(qxp.FillWidget):
	def __init__(self):
		# We will set the color to orange and position it at the center of
		# the screen.
		rect = pygame.Rect(0,0,10,10)
		rect.center = qxp.display.get_rect().center
		qxp.FillWidget.__init__(self, (255,128,0), rect)

		# About the 'rect' member: all widgets have a special instance
		# variable named 'rect'.  In this scope it is accesible as
		# 'self.rect', after calling FillWidget.__init__().  It is a pygame
		# Rect that is used to determine the position and size of the
		# widget.
		
		# 2D movement speed
		self.speed = (0, 0)

	# A couple of utility methods to modify the speed and stop the thingy:
	def set_speed(self, speed):
		self.speed = speed
	
	def stop(self):
		self.speed = (0, 0)

	# Below is the timestep() callback.  When a widget has been added to a
	# WidgetManager, it may receive event callbacks.  The base Widget class
	# is meant as an empty placeholder, and does not define any callbacks.
	# However, when a Widget subclass defines a valid event callback, it
	# will be called when appropriate, and this allows to add behavior to
	# widgets.
	#
	# The timestep() callback is called periodically, allowing the widget
	# to take account of time.  The 'step' argument contains the time
	# interval, in seconds, that passed since the last call to timestep().
	# This value is not guaranteed to be constant, so you must include it
	# in your calculations if you want the widget's behavior to be
	# independent from the FPS rate.  If you really want to simplify your
	# code, you can assume that step will be roughly equal to 1.0 / FPS.
	#
	def timestep(self, step):
		# If we are not moving, do nothing
		if self.speed == (0, 0):
			return

		# Calculate the offset based on the value of self.speed.  We
		# include the step argument in the calculation in order to maintain
		# a constant movement speed regardless of the FPS rate.  The 200
		# constant will provide a 200 pixels / second movement speed.
		offset = [ int(200 * step * v) for v in self.speed ]

		# Modify the rect member.  Whenever the widget needs a display
		# update, it must call self.markdirty() in order to force the
		# rendering of the screen region occupied by self.rect in the next
		# render phase.  The Widget.updaterect() method is a shortcut that
		# replaces self.rect with a new value and marks both (old and new)
		# as dirty.
		rect = self.rect.move(offset)
		self.updaterect(rect)
		#
		# The same effect would have been achieved with:
		#
		# self.markdirty()
		# self.rect.move_ip(offset)
		# self.markdirty()

class Tutorial4(qxp.WidgetManager):
	def __init__(self):
		qxp.WidgetManager.__init__(self)
		self.createlayers('bg', 'fg')

		# Fill the background with a plain color.
		bg = qxp.FillWidget(color=(128,64,0))
		self.addwidget('bg', bg)

		# Side note: this method for filling a background wastes memory,
		# because the FillWidget stores a surface of the size of the
		# screen, painted in the solid color, which is totally unnecessary
		# (unless you want to make it semitransparent).  Later we will
		# learn how to avoid this.
		
		# Add an explanatory label:
		label = qxp.Label('Move the thingy around with the arrow keys.')
		self.addwidget('fg', label)

		# Create the thingy
		thingy = Thingy()
		self.addwidget('fg', thingy)

		# Install the keydown and keyup handlers.  The WidgetManager class
		# provides a handy scheme to handle keyboard events.  The
		# set_keydown_handler() and set_keyup_handler() methods allow to
		# install a callback function for a specific key being pressed or
		# depressed.  They also allow to set any number of arguments that
		# will be passed to the callback.
		for key, speed in (
			(pygame.K_DOWN, (0, 1)),
			(pygame.K_UP, (0, -1)),
			(pygame.K_LEFT, (-1, 0)),
			(pygame.K_RIGHT, (1, 0)),
		):
			self.set_keydown_handler(key, thingy.set_speed, speed)
			self.set_keyup_handler(key, thingy.stop)

if __name__ == '__main__':
	pygame.init()
	qxp.display.init('qxp tutorial #4', (500, 500))
	Tutorial4().loop()

# We're done!  Exercise: The movement is really dumb.  Modify the Thingy
# class to add acceleration and to accept diagonal movement.  Also,
# investigate the keydown() and keyup() callbacks, and modify this example
# to use them instead of the WidgetManager.set_keydown_handler() and
# WidgetManager.set_keyup_handler() methods.

