#!/usr/bin/env python

import pygame
import qxp
from glob import glob

class PresentationScreen(qxp.WidgetManager):
	def __init__(self):
		qxp.WidgetManager.__init__(self)
		self.createlayers('bg', 'fg')
		self.addwidget('bg', qxp.FillWidget((128,64,0)))

		logo = qxp.ImageWidget(
			'qxp.png', alpha=True, center=qxp.display.get_rect().center
		)
		l1 = qxp.Label(
			'This application will run all the tutorials in sequence.',
		)
		l2 = qxp.Label(
			'(Starting from Tutorial #3).',
			topleft=l1.rect.bottomleft,
		)
		l3 = qxp.Label(
			" ",
			topleft=l2.rect.bottomleft,
		)
		l4 = qxp.Label(
			"You can always press 'n' to run the next tutorial, or 'q' to quit.",
			topleft=l3.rect.bottomleft,
		)
		self.addwidget('fg', logo, l1, l2, l3, l4)

		self.set_keydown_handler(pygame.K_n, self.halt)

def run_tutorial(i):
	try:
		f = glob('./tut%d-*.py' % i)[0]
	except:
		return False
	globals = {'__name__': 'x'}
	execfile(f, globals)
	for k, v in globals.iteritems():
		if k.startswith('Tutorial'):
			wm = v()
			wm.set_keydown_handler(pygame.K_n, wm.halt)
			wm.loop()
			break
	return True

if __name__ == '__main__':
	pygame.init()
	qxp.display.init('qxp tutorials', (500, 500))

	PresentationScreen().loop()

	i = 3
	while run_tutorial(i):
		i += 1

