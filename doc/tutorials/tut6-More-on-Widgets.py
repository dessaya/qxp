#!/usr/bin/env python
#
# qxp tutorial #6: More on widgets
# ================================
#
# In this tutorial we will create a completely custom widget that draws a
# circle instead of a rectangle (Wow!! No way!!), using two different
# techniques.

import pygame
import qxp

# Declare the ImageCircle widget class.  This widget will draw a circle
# using the default widget.render() method.  It is the same technique
# used in FillWidget.
#
# This works as follows.  We already learned that all widgets have a rect
# member that determines the geometry of the widget.  There is another
# important member for all widgets: image.  If a widget has both members
# rect and image assigned, it will know how and where to draw itself.  If a
# widget has image = None, the default render() method will pass.
#
class ImageCircle(qxp.Widget):
	def __init__(self, radius, color, **kwargs):
		# Compute the size of self.rect:
		size = (int(2 * radius), int(2 * radius))

		# Create a pygame surface and draw the circle:
		image = pygame.Surface(size, pygame.SRCALPHA).convert_alpha()
		image.fill((0,0,0,0))
		pygame.draw.circle(
			image, color, image.get_rect().center, int(radius),
		)

		# Initialize the widget class, assigning self.image (self.rect
		# is automatically extracted from the image).
		qxp.Widget.__init__(self, image, **kwargs)

		# Side note: What's this 'kwargs' black magic?  You can ignore it,
		# but it allows to apply transformations to the rect in a handy
		# way.  We have been using this feature since tutorial #2, every
		# time we added a transformation to a rect while creating a Widget
		# instance.  Remember this?:
		#
		# image = qxp.ImageWidget(
		# 	'qxp.png', center=qxp.display.get_rect().center,
		# )
		#
		# Here we make use of this 'kwargs' feature by passing the
		# 'center' keyword argument.

# Declare the DynamicCircle class.  Instead of storing a pygame surface in
# self.image, this widget draws the circle dynamically on each render
# phase.
#
class DynamicCircle(qxp.Widget):
	def __init__(self, radius, color, **kwargs):
		# We have to remember the circle properties, because we will draw
		# it later:
		self.radius = int(radius)
		self.color = color

		# We will not assign self.image (it will be set to None), but we
		# still have to assign self.rect, in order to have the render()
		# method called when necessary.
		size = (int(2 * radius), int(2 * radius))
		rect = pygame.Rect((0, 0), size)
		qxp.Widget.__init__(self, rect=rect, **kwargs)
	
	# Override the render() method.  All widgets have a render() method
	# that accepts two arguments:  a surface in which the widget will be
	# drawn (this will normally be the display surface) and a rect that has
	# been marked as dirty.  The render() method is expected to draw the
	# widget *only inside the dirty rect* and leave the rest of the
	# surface as is.  It is guaranteed that the dirty rect is completely
	# inside self.rect, but not that they are the same rect.  This also
	# means that the render() method might be called more than once in the
	# same render phase for two different dirty rects inside self.rect.
	#
	def render(self, surface, dirtyrect):
		# Use the Surface.set_clip() method to guarantee that we will only
		# modify the area inside the dirty rect.
		surface.set_clip(dirtyrect)
		
		# Draw the circle.
		pygame.draw.circle(
			surface, self.color, self.rect.center, self.radius,
		)

		# Remove the clipping region.
		surface.set_clip()

class Tutorial6(qxp.WidgetManager):
	def __init__(self):
		qxp.WidgetManager.__init__(self)
		self.createlayers('bg', 'fg')

		# Fill the background.
		bg = qxp.FillWidget(color=(128,64,0))
		self.addwidget('bg', bg)

		# Create an ImageCircle instance:
		icircle = ImageCircle(30, (255,128,0), center=(250, 166))
		self.addwidget('fg', icircle)

		# Add a label for the ImageCircle:
		label = qxp.Label(
			'This is the ImageCircle', midtop=icircle.rect.midbottom,
		)
		self.addwidget('fg', label)

		# Create a DynamicCircle instance:
		dcircle = DynamicCircle(30, (255,128,0), center=(250, 333))
		self.addwidget('fg', dcircle)

		# Add a label for the DynamicCircle:
		label = qxp.Label(
			'This is the DynamicCircle', midtop=dcircle.rect.midbottom,
		)
		self.addwidget('fg', label)

if __name__ == '__main__':
	pygame.init()
	qxp.display.init('qxp tutorial #6', (500, 500))
	Tutorial6().loop()

# End of tutorial.  Here we learned two diferent techniques for rendering a
# widget.  Both techniques have their pros and cons, and it's up to you to
# decide which one to use for your widgets:
#
# self.image technique:
#   * Easy to implement: draw once in the __init__() method.
#   * Consumes memory: stores a pygame surface that is blitted
#     automatically in the default render() method.
#   * The surface can have any of the three transparency types
#     implemented in Pygame.
#   * To change the widget's appearance you must modify or replace
#     self.image and call self.markdirty().
#   * If qxp ever implements a hardware accelerated mode, this technique
#     is guaranteed to work in either software or hardware accelerated
#     modes.
#
# render() technique:
#   * Somewhat harder to implement: the widget has to remember the
#     information needed to draw itself.
#   * Consumes CPU cycles: for complex widgets it will take more CPU cycles
#     on each render phase than it takes for just blitting a static image.
#   * To change the appearance you must modify the state variables used in
#     render(), and then call self.markdirty().
#   * This technique depends on the pygame ability to draw directly on
#     the display surface.  In the near future it is planned to add support
#     for a hardware accelerated mode in qxp.  If this ever happens, care
#     will have to be taken inside the render() method so as to make it
#     work in both software and hardware accelerated modes.
#
# Exercise: Create the DynamicFillWidget class.  Its behavior should mimic
# the FillWidget, but it should use the dynamic technique instead.  Try to
# write the render() method in only one line, without using set_clip().

