#!/usr/bin/env python
#
# qxp tutorial #3: Subclassing qxp
# ================================
#
# The first tutorials introduced a few of the basic concepts of qxp.  This
# was accomplished by directly creating instances of WidgetManager and some
# widgets.  However, in a normal qxp application it will be sometimes more
# convenient to subclass the qxp classes, to easily customize their
# behavior.  All qxp classes can be subclassed, including WidgetManager,
# Widget, Animation, etc.
#
# This tutorial will create the same exact scene created for Tutorial #2,
# but using a custom WidgetManager class.

import pygame
import qxp

# Here we declare our custom widget manager, named Tutorial3.
class Tutorial3(qxp.WidgetManager):
	
	# We create the layers and widgets inside the __init__() method:
	def __init__(self):
		# Initialize the WidgetManager base class:
		qxp.WidgetManager.__init__(self)

		# And the same stuff as before, replacing 'wm' with 'self':
		self.createlayers('background', 'foreground')
		image = qxp.ImageWidget(
			'qxp.png', alpha=True, center=qxp.display.get_rect().center
		)
		self.addwidget('foreground', image)
		rectangle = qxp.FillWidget(color=(128,64,0))
		self.addwidget('background', rectangle)
		label = qxp.Label(
			'This is the qxp logo.', midtop=image.rect.midbottom
		)
		self.addwidget('foreground', label)
		fps_meter = qxp.FPSWidget()
		self.addwidget('foreground', fps_meter)

# And the action takes place inside the 'pythonic main() function':
if __name__ == '__main__':
	pygame.init()
	qxp.display.init('qxp tutorial #3', (500, 500))
	Tutorial3().loop()

