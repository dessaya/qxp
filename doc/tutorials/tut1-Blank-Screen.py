#!/usr/bin/env python
#
# qxp tutorial #1: Blank screen
# =============================
#
# Welcome to the qxp tutorials!  We will begin with a simple task:
# create a useless black window.  Watch 'n learn.

# qxp is not a replacement for the pygame package: we still have to
# import it, as we will use several of its classes.  If you are not
# familiar with Pygame, go read a tutorial and come back.
import pygame

# Import the qxp extensions.
# It is also allowed to use the 'from qxp import *' form, but we want
# to keep our namespace clean and tidy, don't we?
import qxp

# Initialize pygame.
pygame.init()

# We use qxp to create the window with a certain caption and size.
qxp.display.init('qxp tutorial #1', (500, 500))

# Create a widget manager.  This object provides methods to manage widgets,
# layers, animations, events and rendering.
wm = qxp.WidgetManager()

# Start the main loop. We will give a maximum rate of 30 frames per second.
# However, qxp uses the 'dirty rects' mechanism to determine when and where
# an update is needed in the display, and since no widgets have been added,
# the screen will be rendered only once (the plain black background) and
# never again.  Check your CPU usage to confirm that the main loop is not
# wasting CPU cycles.
wm.loop(fps=30)

# Usually you can omit the 'fps' argument.  The default value is 50 FPS.

# That's it! Close the window or press 'q' to quit. You can also press 'f'
# to toggle fullscreen mode.  (If fullscreen does not work, try changing
# the screen size to a more decent value, like 800x600).

