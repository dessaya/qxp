#!/usr/bin/env python
#
# qxp tutorial #2: Add some widgets
# =================================
#
# Whenever you want to draw things on screen, you will use a qxp Widget.
# Be it the background color, a character, a button... even the mouse
# cursor is a Widget.
#
# The qxp Widget is similar to the pygame Sprite, but with some advantages
# (and disadvantages).  Widgets can be stacked up one over the other,
# and they will always draw correctly and in the same order, even if
# the background is changing.  Widgets are also tightly integrated
# in the qxp event system, and it is very easy to catch an event inside
# a Widget subclass.
#
# On the (potentially) down side, qxp does not provide any of the grouping
# mechanisms used for pygame Sprites.  Also, no collision detection
# mechanism is provided (yet).
#
# Let's see how to use some of the built-in Widgets.

import pygame
import qxp

pygame.init()
qxp.display.init('qxp tutorial #2', (500, 500))
wm = qxp.WidgetManager()

# qxp uses layers to arrange widgets.  This allows to easily determine the
# drawing order of the widgets, regardless of the order in which they were
# added.

# First we must create some layers.
wm.createlayers('background', 'foreground')

# You can give any meaningful name to a layer, and you can create as many
# layers as you need.  In this example, we will have two layers named
# 'background' and 'foreground'.  Given the order in which they were
# created, all widgets added to the 'foreground' layer will be drawn after
# all widgets added to the 'background' layer.  Widgets in the same layer
# will be drawn in the same order they were added.

# Create an image widget to show the qxp logo:
image = qxp.ImageWidget(
	'qxp.png', alpha=True, center=qxp.display.get_rect().center
)

# Add the image to the foreground layer:
wm.addwidget('foreground', image)

# Create a widget to fill the background with a plain color.  By default,
# this widget extends to the full screen.
rectangle = qxp.FillWidget(color=(128,64,0))

# Add the rectangle to the background layer:
wm.addwidget('background', rectangle)

# Create a label in the foreground layer.
label = qxp.Label('This is the qxp logo.', midtop=image.rect.midbottom)
wm.addwidget('foreground', label)

# Create the special FPS meter widget and add it to the 'foreground' layer.
fps_meter = qxp.FPSWidget()
wm.addwidget('foreground', fps_meter)

wm.loop()

# End of tutorial #2.  Note how the colored rectangle is
# effectively drawn below the other widgets.

# One final observation: the FPS meter should mark around 50.0 frames per
# second.  But this is a static scene, and there is no need to render 50
# times a second.  As in the previous tutorial, rendering only once should
# be enough.  So, why is the meter marking 50 FPS?  Because this special
# widget always marks itself as dirty, so the main loop is forced to render
# at least the small screen space ocuppied by the FPS widget, 50 times per
# second.  If we had not added the FPS meter, the scene would have been
# drawn once.  It's like quantum mechanics: you cannot measure the FPS rate
# without modifying it =).

