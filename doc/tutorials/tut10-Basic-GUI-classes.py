#!/usr/bin/env python
#
# qxp tutorial #10: Basic GUI classes
# ===================================
#
# The qxp widgets subsystem allows, in theory, to implement a rich set of
# GUI (graphical user interface) classes for interaction with the user.
#
# In practice, since qxp is in an early production stage, only a few of
# these classes have been implemented, and with a minimal set of features.

import pygame
import qxp

class Tutorial10(qxp.WidgetManager):
	def __init__(self):
		qxp.WidgetManager.__init__(self)
		self.createlayers('bg', 'fg')
		self.addwidget('bg', qxp.FillWidget((128, 64, 0)))

		# We are already familiar with the Label widget:
		self.label = qxp.Label('This is a label.')

		# This is just a pygame rect that we will use to add some space
		# between widgets.
		space = pygame.Rect(0, 0, 10, 20)
		space.bottomright = qxp.display.get_rect().bottomright
	
		# The Button widget is the base class for all widgets that respond
		# to mouse clicking and hovering.  The Button supports three
		# states: 'normal', 'hover' and 'pressed', and each state is
		# associated with a pygame surface.
		#
		# In this case we create a Button with only two states: 'normal'
		# and 'hover'.  When the button is clicked, it will call
		# WidgetManager.halt(), which finishes the main loop.
		exitbutton = qxp.Button(
			normalsurface='exit.png',
			hoversurface='exit-hover.png',
			func=self.halt,
			bottomright=space.topright,
		)

		# The TextButton is just a shortcut that automatically creates the
		# necessary surfaces for the Button widget, with some text.  It
		# doesn't draw a frame around the text, though.
		#
		# This widget will call self.button_click(1) when clicked.  Note
		# that the 'funcargs' argument expects to receive a list or tuple.
		button1 = qxp.TextButton(
			'Button 1',
			func=self.button_click,
			funcargs=(1,),
			center=qxp.display.get_rect().center,
		)

		space.midtop = button1.rect.midbottom

		# And this TextButton will call self.button_click(2).
		button2 = qxp.TextButton(
			'Button 2',
			func=self.button_click,
			funcargs=(2,),
			midtop=space.midbottom,
		)
		
		space.midtop = button2.rect.midbottom
		
		# qxp also provides the CheckButton as a base class to implement a
		# true-false widget.  Just as the Button Widget, it associates each
		# one of its states with a pygame surface.
		#
		# The TextCheckButton is a shortcut for the CheckButton that draws
		# an ugly checkbox and some text beside it.
		self.checkbox = qxp.TextCheckButton(
			'Check button',
			func=self.checkbutton_click,
			midtop=space.midbottom,
		)
		
		# Note how the addwidget() method can receive many widgets at once:
		self.addwidget(
			'fg',
			self.label, button1, button2, self.checkbox, exitbutton,
		)

	def button_click(self, number):
		self.label.text = 'Button %d clicked!' % (number,)

	def checkbutton_click(self):
		state = ('off', 'on')[self.checkbox.value]
		self.label.text = 'The check button is %s.' % (state,)

if __name__ == '__main__':
	pygame.init()
	qxp.display.init('qxp tutorial #10', (500, 500))
	Tutorial10().loop()

