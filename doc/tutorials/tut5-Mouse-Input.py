#!/usr/bin/env python
#
# qxp tutorial #5: Mouse input
# ============================
#
# In this tutorial we will use a widget similar to the one we created in
# the last tutorial, but in this case we will make it respond to mouse
# events.

import pygame
import qxp

# In this tutorial we will be doing some math.
import math

# Wait!  Come back!  Only a square root.  It's a promise.

class Thingy(qxp.FillWidget):
	def __init__(self):
		rect = pygame.Rect(0,0,10,10)
		rect.center = qxp.display.get_rect().center
		qxp.FillWidget.__init__(self, (255,128,0), rect)
		self.speed = (0, 0)
		# Destination coordinates:
		self.destination = self.rect.center

	def update_speed(self):
		# This is a method that calculates self.speed to make the thingy
		# always move towards the destination.
		if self.rect.center == self.destination:
			self.speed = (0, 0)
			return
		mx, my = self.destination
		rx, ry = self.rect.center
		dx, dy = float(mx - rx), float(my - ry)
		dist = math.sqrt(dx ** 2 + dy ** 2)
		self.speed = dx / dist, dy / dist

	# Here we make the thingy always move towards its destination.
	def timestep(self, step):
		# Update the speed value and then calculate the new position for
		# self.rect.
		self.update_speed()
		if self.speed == (0, 0):
			return
		offset = [ int(200 * step * v) for v in self.speed ]
		rect = self.rect.move(offset)
		self.updaterect(rect)

	# Define the mousebuttondown() callback.  Just like the timestep(),
	# keydown() and keyup() callbacks, any widget may define the
	# mousebuttondown() callback that will be called whenever a mouse
	# button is pressed.  It receives the mouse cursor coordinates and the
	# pygame identifier for the mouse button.
	def mousebuttondown(self, pos, button):
		# Set the destination:
		self.destination = pos

class Tutorial5(qxp.WidgetManager):
	def __init__(self):
		qxp.WidgetManager.__init__(self)
		self.createlayers('bg', 'fg')

		bg = qxp.FillWidget(color=(128,64,0))
		self.addwidget('bg', bg)

		label = qxp.Label('Click anywhere on the screen.')
		self.addwidget('fg', label)

		thingy = Thingy()
		self.addwidget('fg', thingy)

if __name__ == '__main__':
	pygame.init()
	qxp.display.init('qxp tutorial #5', (500, 500))
	Tutorial5().loop()

# That's it.  Exercise: Once again, the movement is awkward.  Modify the
# Thingy class to make it move _really_ towards the destination, and make
# its movement speed proportional to the distance.  Also, investigate the
# mousemotion() and mousebuttonup() callbacks.

