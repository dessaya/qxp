#!/usr/bin/env python
#
# qxp tutorial #8: Animations -- Part 2
# =====================================
#
# In the last tutorial we learned about the RectAnimation class and how it
# creates an animation that modifies the properties of a widget's rect
# member.  However, the Animation subsystem is generic in nature and allows
# to define custom animations, by subclassing any of the base animation
# classes.
#
# In this tutorial we will mix up some of the previous tutorials.  We will
# take the DynamicCircle from the tutorial #6, we will make it respond to
# the mouse button like in the tutorial #7, but we will make it more
# expressive in this case: we will create a ColorAnimation and learn how to
# use the Sequence animation.

import pygame
import qxp

# This is just a utility function that interpolates between two colors,
# with or without alpha value.  It returns a new color that is between both
# colors.
def blendcolor(color1, color2, blend):
	"""blend = 0.0 -> color1; blend = 1.0 -> color2"""
	return tuple(map(
		lambda c1, c2: max(0, min(255, int(c1 + (c2 - c1) * blend))),
		color1, color2
	))


# Declare our new ColorAnimation class.  It derives from the
# qxp.LerpAnimation.  The LerpAnimation ('Linear intERPolation') is a
# generic animation class that interpolates a float value between two given
# values, for a given time interval.  All derived classes implement
# extended functionality.  For example, the RectAnimation also derives from
# LerpAnimation.
class ColorAnimation(qxp.LerpAnimation):
	def __init__(self, widget, duration, destcolor, srccolor=None):
		# We tell the LerpAnimation class to interpolate between 0.0 and
		# 1.0.  This is because in a color we have three values to
		# interpolate (four if we have alpha), and if we want LerpAnimation
		# to do all the work, that would require three (or four)
		# LerpAnimation instances.  Instead, we will do the dirty work
		# inside the timestep() callback.
		qxp.LerpAnimation.__init__(self, duration, 1.0, 0.0)
		self.widget = widget
		self.destcolor = destcolor
		self.srccolor = srccolor

	# Override the start() callback.  This method is called when the
	# animation is triggered.
	def start(self):
		# If the srccolor member is not set, we set it to the current
		# widget color.  We didn't do this in the __init__() method because
		# the animation can be triggered any time and the widget color
		# might have changed.
		if self.srccolor is None:
			self.srccolor = self.widget.color
		else:
			# Just in case the source color differs from the current widget
			# color; we set the color and mark dirty.
			self.widget.color = self.srccolor
			self.widget.markdirty()

	# Override the timestep() callback.  The timestep() callback works in
	# animations just like it does for widgets.
	def timestep(self, step):
		# First we call the timestep() method for the base LerpAnimation
		# class.  Normally timestep() methods return None, but in the case
		# of LerpAnimation, it has a special behavior: it returns the
		# current interpolation value.  In this case it is a value between
		# 0.0 and 1.0.
		blend = qxp.LerpAnimation.timestep(self, step)

		# Calculate the interpolated color.
		color = blendcolor(self.srccolor, self.destcolor, blend)

		# Assign the new color to the widget and mark dirty.
		self.widget.color = color
		self.widget.markdirty()

class DynamicCircle(qxp.Widget):
	def __init__(self, radius, color, **kwargs):
		self.radius = int(radius)
		self.color = color
		self.initial_color = color
		size = (int(2 * radius), int(2 * radius))
		rect = pygame.Rect((0, 0), size)
		qxp.Widget.__init__(self, rect=rect, **kwargs)
		self.animation = None

	def render(self, surface, dirtyrect):
		surface.set_clip(dirtyrect)
		pygame.draw.circle(
			surface, self.color, self.rect.center, self.radius,
		)
		surface.set_clip()

	# Declare a shortcut to create a ColorAnimation instance.
	# Not really necessary; it saves from typing this:
	#
	#    anim = ColorAnimation(self, ...)
	#
	# and allows to type this:
	#
	#    anim = self.ColorAnimation(...)
	#
	# which is not really shorter, but looks more object oriented =)
	#
	def ColorAnimation(self, *args, **kwargs):
		return ColorAnimation(self, *args, **kwargs)

	def mousebuttondown(self, pos, button):
		if self.animation is not None and not self.animation.finished:
			self.wm.animations.remove(self.animation)
		destrect = pygame.Rect(self.rect)
		destrect.center = pos
		
		# Create the 'move' animation:
		move = self.RectAnimation(1.0, destrect, easeout=True)

		# Create an animation that changes the color to green:
		green = self.ColorAnimation(0.25, (193,231,53))

		# Create an animation that changes the color back to its initial
		# value:
		reset_color = self.ColorAnimation(0.25, self.initial_color)

		# We want to execute the three animations we just created in
		# sequence.  How do we achieve this?  Enter the Sequence.  We group
		# the three animations into the special Sequence animation and
		# voila!
		seq = qxp.Sequence(move, green, reset_color)
		
		self.animation = seq
		self.wm.animations.add(seq)

class Tutorial8(qxp.WidgetManager):
	def __init__(self):
		qxp.WidgetManager.__init__(self)
		self.createlayers('bg', 'fg')

		bg = qxp.FillWidget(color=(128,64,0))
		self.addwidget('bg', bg)

		label = qxp.Label('Click anywhere on the screen.')
		self.addwidget('fg', label)

		circle = DynamicCircle(
			10, (255,128,0), center=qxp.display.get_rect().center,
		)
		self.addwidget('fg', circle)

if __name__ == '__main__':
	pygame.init()
	qxp.display.init('qxp tutorial #8', (500, 500))
	Tutorial8().loop()

# That's All Folks(tm)!.  In this tutorial we learned about the
# LerpAnimation and how to subclass it, and we also discovered the Sequence
# animation.  Excercise: If you click while the ball is green, it will remain
# green, which might be considered a bug if we expect the ball to be orange
# while it is traveling.  Try to fix this.  Also, check out the Parallel
# animation, which allows to execute a group of animations in parallel, the
# Wait animation, which does absolutely nothing for a time interval, and
# the Func animation, which calls an arbitrary function when triggered.

